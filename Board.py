from Coordinate import Coordinate
from Words import Words

class Board:

	def __init__(self, letters):
		self.board = [['', '', '', ''],
		              ['', '', '', ''],
		              ['', '', '', ''],
		              ['', '', '', '']]
		self.loadBoard(letters)
		self.words = Words()

	def loadBoard(self, letters):
		if len(letters) != 16:
			raise ValueError("Input should be of length 16!")
		letters = letters.lower()
		for i in range(len(letters)):
			letter = letters[i]
			x = i%4
			y = int(i/4)
			self.board[x][y] = letter

	@staticmethod
	def getStartCoordinates():
		result = []
		for i in range(4):
			for j in range(4):
				result.append(Coordinate(i, j))
		return result

	def getChar(self, coordinate):
		return self.board[coordinate.x][coordinate.y]

	def findWords(self):
		result = set()
		for startCoord in Board.getStartCoordinates():
			result.update(self.search(startCoord, self.words.tree))
		result = list(result)
		result.sort()
		return result

	def printWords(self):
		words = self.findWords()
		result = {}
		for word in words:
			l = len(word)
			if l not in result:
				result[l] = [word]
			else:
				result[l].append(word)

		totalWords = 0
		keys = list(result.keys())
		keys.sort()
		for key in keys:
			numWords = len(result[key])
			totalWords += numWords
			print(str(numWords) + " words with length " + str(key) + ":")
			for word in result[key]:
				print("\t" + word)
		print("Total number of words: " + str(totalWords))

	def search(self, coordinate, tree, visited=set()):
		char = self.getChar(coordinate)
		visited = set(visited)
		visited.add(coordinate)
		result = set()
		if char in tree:
			if tree[char]['isWord']:
				result.add(tree[char]['word'])

			for neighbour in coordinate.getNeighbours():
				if neighbour not in visited:
					result.update(self.search(neighbour, tree[char]['nextChars'], visited))
		return result

b = Board("tbsierehtpadylsn")
b.printWords()