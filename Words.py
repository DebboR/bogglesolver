
FILE_NAME = "Boggle.txt"

class Words:

	def __init__(self):
		self.tree = {}
		self.importWords()

	def importWords(self):
		f = open(FILE_NAME, 'r')
		for line in f:
			self.importWord(line.strip())
		f.close()


	def importWord(self, word):
		tempPointer = self.tree
		for i in range(len(word)):
			char = word[i]
			if char not in tempPointer:
				tempPointer[char] = {'isWord': False, 'nextChars': {}}
			if i == len(word) - 1:
				tempPointer[char]['isWord'] = True
				tempPointer[char]['word'] = word
			tempPointer = tempPointer[char]['nextChars']

	def isWord(self, word):
		tempPointer = self.tree
		for i in range(len(word)):
			char = word[i]
			if char not in tempPointer:
				return False
			if i == len(word) - 1:
				return tempPointer[char]['isWord']
			tempPointer = tempPointer[char]['nextChars']



