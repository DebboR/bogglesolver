class Coordinate:

	def __init__(self, x, y):
		self.x = x
		self.y = y

	def __eq__(self, other):
		return isinstance(other, self.__class__) and other.x == self.x and other.y == self.y

	def __ne__(self, other):
		return not self.__eq__(other)

	def __str__(self):
		return "Coord: " + str(self.x) + ", " + str(self.y)

	def __hash__(self):
		return 4*self.x + self.y

	@staticmethod
	def isValid(x, y):
		return 0 <= x < 4 and 0 <= y < 4

	def getNeighbours(self):
		result = []
		for i in range(-1, 2):
			for j in range(-1, 2):
				if Coordinate.isValid(self.x + i, self.y + j) and not (i == 0 and j == 0):
					result.append(Coordinate(self.x + i, self.y + j))
		return result


